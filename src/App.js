import React from 'react';
import {AppBar, Button, Toolbar, Typography} from '@material-ui/core';
import XmlTree from './XmlTree';
import './App.css';

class App extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      data: [{
        id: 0,
        name: "root",
        children: [{
          id: 1,
          name: "things 1",
          attribute: [],
          children: [],
          text: null
        },
        {
          id: 2,
          name: "things 2",
          attribute: [],
          children: [
            {
              id: 3,
              name: "cat",
              attribute: [],
              children: [],
              text: null
            },
            {
              id: 4,
              name: "dog",
              attribute: [
                {
                  name: "attr1",
                  value: "value"
                },
                {
                  name: "attr2",
                  value: "value"
                }
              ],
              children: [],
              text: null
            }
          ],
          text: null
        }],
        attribute: [{
          name: "score",
          value: 10
        },
        {
          name: "special",
          value: "true"
        }],
        text: null
      }],
    };
  }

  addItem = (id, item)=>{
    // let data = this.state.data;
    // let node = data.filter(item=>{
    //   return item.id == id
    // });
    // node[0].children.push({
    //   id: null,
    //   name: item.name,
    //   text: item.text,
    //   attribute: item.attribute
    // })
  }

  render(){
    return (
      <div className="appbar-base">
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className="appbar-title">
              Simple XML Editor
            </Typography>
            <Button color="inherit" className="appbar-menuButton">Import</Button>
            <Button color="inherit" className="appbar-menuButton">Export</Button>
          </Toolbar>
        </AppBar>
        
        <Button>Import</Button>
        <Button>Export</Button>
        <Button>Graph</Button>

        <XmlTree data={this.state.data} addItem={this.addItem}/>
      </div>
    );
  }
}

export default App;
