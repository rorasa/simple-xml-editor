import React from 'react';
import { Button, Grid,
    Dialog,
    DialogTitle,
    TextField,
    Typography,
    DialogActions,
    ExpansionPanel, 
    ExpansionPanelSummary, 
    ExpansionPanelDetails} from '@material-ui/core';

class NodeDialog extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            name: null,
            attribute1: {
                key: null,
                value: null
            },
            attribute2: {
                key: null,
                value: null
            },
            attribute3: {
                key: null,
                value: null
            },
            attribute4: {
                key: null,
                value: null
            },
            text: null,
        }
    }

    render(){
        return(
            <Dialog open={this.props.showAddNodeDialog} className="dialog-addnode-base">
                <DialogTitle>Add new node</DialogTitle>
                <Grid container>
                    <Grid item xs={12}>
                        <TextField
                        label="Node name"
                        text={this.state.name}
                        onChange={(e)=>{
                            this.setState({
                                name: e.target.value
                            });
                        }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                        label="Text"
                        multiline
                        text={this.state.text}
                        onChange={(e)=>{
                            this.setState({
                                text: e.target.value
                            })
                        }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="h6">Attributes</Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                        label="Atrribute1 name"
                        text={this.state.attribute1.key}
                        onChange={(e)=>{
                            let attr = this.state.attribute1;
                            attr.key = e.target.value;
                            this.setState({
                                attribute1: attr
                            });
                        }}
                        />
                        <TextField
                            label="Atrribute1 value"
                            text={this.state.attribute1.value}
                            onChange={(e)=>{
                                let attr = this.state.attribute1;
                                attr.value = e.target.value;
                                this.setState({
                                    attribute1: attr
                                });
                            }}
                        />
                        <TextField
                            label="Atrribute2 name"
                            text={this.state.attribute2.key}
                            onChange={(e)=>{
                                let attr = this.state.attribute2;
                                attr.key = e.target.value;
                                this.setState({
                                    attribute2: attr
                                });
                            }}
                        />
                        <TextField
                            label="Atrribute2 value"
                            text={this.state.attribute2.value}
                            onChange={(e)=>{
                                let attr = this.state.attribute2;
                                attr.value = e.target.value;
                                this.setState({
                                    attribute2: attr
                                });
                            }}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                        label="Atrribute3 name"
                        text={this.state.attribute3.key}
                        onChange={(e)=>{
                            let attr = this.state.attribute3;
                            attr.key = e.target.value;
                            this.setState({
                                attribute3: attr
                            });
                        }}
                        />
                        <TextField
                            label="Atrribute3 value"
                            text={this.state.attribute3.value}
                            onChange={(e)=>{
                                let attr = this.state.attribute3;
                                attr.value = e.target.value;
                                this.setState({
                                    attribute3: attr
                                });
                            }}
                        />
                        <TextField
                            label="Atrribute4 name"
                            text={this.state.attribute4.key}
                            onChange={(e)=>{
                                let attr = this.state.attribute4;
                                attr.key = e.target.value;
                                this.setState({
                                    attribute4: attr
                                });
                            }}
                        />
                        <TextField
                            label="Atrribute4 value"
                            text={this.state.attribute4.value}
                            onChange={(e)=>{
                                let attr = this.state.attribute4;
                                attr.value = e.target.value;
                                this.setState({
                                    attribute4: attr
                                });
                            }}
                        />
                    </Grid>
                </Grid>
                
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.closeAddNodeDialog}>Cancel</Button>
                    <Button variant="contained" color="primary" onClick={()=>{
                        let attributes = []
                        if (this.state.attribute1.key != null){
                            attributes.push({
                                name: this.state.attribute1.key,
                                value: this.state.attribute1.value
                            });
                        }
                        if (this.state.attribute2.key != null){
                            attributes.push({
                                name: this.state.attribute2.key,
                                value: this.state.attribute2.value
                            });
                        }
                        if (this.state.attribute3.key != null){
                            attributes.push({
                                name: this.state.attribute3.key,
                                value: this.state.attribute3.value
                            });
                        }
                        if (this.state.attribute4.key != null){
                            attributes.push({
                                name: this.state.attribute4.key,
                                value: this.state.attribute4.value
                            });
                        }
                        this.props.addItem(this.props.nodeId, {
                            name: this.state.name,
                            text: this.state.text,
                            attribute: attributes
                        })
                    }}>Show</Button>
                </DialogActions>
            </Dialog>
        )
    }
}
class Node extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            showAddNodeDialog: false
        };
    }

    openAddNodeDialog = () =>{
        this.setState({
            showAddNodeDialog: true
        })
    }

    closeAddNodeDialog = () =>{
        this.setState({
            showAddNodeDialog: false
        });
    }

    render(){
        const attributes = this.props.item.attribute.map(attr=>{
            return (<li>{attr.name}: {attr.value}</li>)
        });
        const children = this.props.item.children.map(item=>{
            return <Node item={item} addItem={this.props.addItem}/>
        })

        return (
            <ExpansionPanel>
                <ExpansionPanelSummary className="node-base">
                    <div className="node-title">{this.props.item.name}</div>
                    <Button className="node-menuButton">Edit</Button>
                    <Button className="node-menuButton" onClick={this.openAddNodeDialog}>+</Button>
                    <Button className="node-menuButton">-</Button>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Grid container>
                        <Grid item xs={12}>
                            <ul>
                                {attributes}
                            </ul>
                        </Grid>
                        <Grid item xs={12}>
                            {children}
                        </Grid>
                    </Grid>
                </ExpansionPanelDetails>

                <NodeDialog 
                    showAddNodeDialog={this.state.showAddNodeDialog} 
                    closeAddNodeDialog={this.closeAddNodeDialog}
                    nodeId={this.props.item.id}
                    addItem={this.props.addItem}
                />
            </ExpansionPanel>
        );
    }
}

class XmlTree extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        const nodes = this.props.data.map(item=>{
            return <Node item={item} addItem={this.props.addItem}/>
        })

        return(
            <div>
                {nodes}
            </div>
        );
    }
}

export default XmlTree;